import React from 'react';
import './SearchBar.css';

function SearchBar(props) {

  const filterText = props.filterText;
  const inStockOnly = props.inStockOnly;

  return (
    <form>
      <input 
        type="text" 
        placeholder="Search..." 
        value={filterText} 
        onChange={changing => props.onChangeFilter(changing.target.value)}
      />
      <p>
        <input 
          type="checkbox" 
          checked={inStockOnly}
          onChange={(changing => props.onChangeStock(changing.target.checked))}  
        />
        {' '}
        Only show products in stock
      </p>
    </form>
  );
}

export default SearchBar;