import React, { useState } from 'react';
import './FilterableProductTable.css';
import SearchBar from '../searchBar/SearchBar';
import ProductTable from '../productTable/ProductTable';

function FilterableProductTable() {

  const [filterText, setFilterText] = useState('');
  const [inStockOnly, setInStockOnly] = useState(false);

  return (
    <div>
      <SearchBar 
        filterText={filterText} 
        inStockOnly={inStockOnly} 
        onChangeFilter={(changing) => setFilterText(changing)}
        onChangeStock={(changing) => setInStockOnly(changing)}
      />
      <ProductTable filterText={filterText} inStockOnly={inStockOnly} />
    </div>
  );
}

export default FilterableProductTable;